package com.java.nttdata.config;

import com.java.nttdata.model.Client;
import com.java.nttdata.repository.ClientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;


@Component
public class JwtFilterRequest extends OncePerRequestFilter {

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private ClientRepository clientRepository;

    private static Logger log = LoggerFactory.getLogger(JwtFilterRequest.class);
    private static final List<String> EXCLUDE_URL = Arrays.asList("/subs");

    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authorizationHeader = request.getHeader("Authorization");
        String userName = null;
        String jwtToken = null;
        log.info("FILTER");
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
            jwtToken = authorizationHeader.substring(7);
            userName = jwtUtils.extractUsernane(jwtToken);
        }

        if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null ){
            List<Client> clients = clientRepository.findAll();
            Client foundclient =null;
            for (Client client:clients) {
                if (Objects.equals(client.getApellido(), userName)){
                    foundclient= client;
                }
            }
            User user = new User(foundclient.getApellido(), String.valueOf(foundclient.getId()), new ArrayList<>());
            Boolean tockenValidated  = jwtUtils.validateToken(jwtToken, user);
            if (Boolean.TRUE.equals(tockenValidated)) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =new UsernamePasswordAuthenticationToken(user, null, ((UserDetails) user).getAuthorities());
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
        filterChain.doFilter(request, response);
    }


    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        System.out.println(request.getServletPath());
        return EXCLUDE_URL.stream().anyMatch(exclude -> exclude.equalsIgnoreCase(request.getServletPath()));
    }
}
