package com.java.nttdata.controller;

import com.java.nttdata.config.JwtUtils;
import com.java.nttdata.model.Login;
import com.java.nttdata.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Controller Class
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@Slf4j
public class LoginController {

    @Autowired
    LoginService loginService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtils jwtlUtils;

    /**
     * Post Validate Credentials Login Client
     * @param login info login
     * @return Result info
     */
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("user")
    public ResponseEntity<String> postValidateUser(@RequestBody Login login){
        log.info("Arrived Request Validate");
        String validClient = loginService.getValidClient(login.getUsername(), login.getPassword());

        if (Objects.equals(validClient, "valid")){
            User user = new User(login.getUsername(), String.valueOf(login.getPassword()), new ArrayList<>());
            Token token = new Token(jwtlUtils.generateToken(user));
            return new ResponseEntity<>(token.getAccessToken(),HttpStatus.OK);
        }else{
            return new ResponseEntity<>("Not Found",HttpStatus.NOT_FOUND);
        }
    }

    public class Token
    {
        String accessToken;

        public Token( String accessToken ){
            this.accessToken = accessToken;
        }

        public String getAccessToken(){
            return accessToken;
        }

        public void setAccessToken( String accessToken ){
            this.accessToken = accessToken;
        }
    }

}
