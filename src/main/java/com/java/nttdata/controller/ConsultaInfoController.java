package com.java.nttdata.controller;


import com.java.nttdata.model.Client;
import com.java.nttdata.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Controller Class
 */
@RestController
@RequestMapping("/client")
@Slf4j
public class ConsultaInfoController {

    @Autowired
    CustomerService customerService;

    /**
     * Get Client By Id
     * @param id id Client
     * @return Info Client
     */
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/{id}")
    public ResponseEntity<Client> getClientById(@PathVariable("id") long id){
        log.info("Arrived Request Get Client");
        Client client = customerService.getClient(id);
        if (client!=null) {
            return new ResponseEntity<>(client, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new Client(),HttpStatus.NOT_FOUND);
        }
    }
}
