package com.java.nttdata.service;

import com.java.nttdata.model.Client;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * Service Class
 */
@Service
@Slf4j
public class LoginService {

    @Autowired
    CustomerService customerService;

    /**
     * Valid info Client Credentials
     * @param username the username
     * @param password the password
     * @return Info Validate
     */
    public String getValidClient(String username, Long password){
        String clientValid = "invalid";

        Client clientData = customerService.getClient(password);

        if(clientData!=null
                && Objects.equals(username, clientData.getApellido())
                && Objects.equals(password, clientData.getId())){
            clientValid = "valid";
        }
        return clientValid;
    }
}
