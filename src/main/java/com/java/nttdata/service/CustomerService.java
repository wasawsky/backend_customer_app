package com.java.nttdata.service;

import com.java.nttdata.model.Client;
import com.java.nttdata.repository.ClientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class CustomerService {

    @Autowired
    ClientRepository clientRepository;

    public Client getClient(Long id){
        Optional<Client> client = clientRepository.findById(id);
        return client.orElse(null);
    }
}
