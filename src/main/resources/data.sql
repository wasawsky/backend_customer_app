create table clients (
	id INT,
	nombre VARCHAR(50),
	apellido VARCHAR(50),
	telefono INT,
	oficio VARCHAR(50),
	direccion VARCHAR(50),
	ciudad VARCHAR(50),
	email VARCHAR(50)
);
insert into clients (id, nombre, apellido, telefono, oficio, direccion, ciudad, email) values (1000, 'Michael', 'Ballesteros', 17592, 'Electrician', '08 Heath Center', 'Sadabe', 'lcherrington0@si.edu');
insert into clients (id, nombre, apellido, telefono, oficio, direccion, ciudad, email) values (2000, 'Odetta', 'Showl', 19374, 'Construction Expeditor', '15 South Point', 'Correntina', 'oshowl1@pbs.org');
insert into clients (id, nombre, apellido, telefono, oficio, direccion, ciudad, email) values (3000, 'Carlin', 'Tomini', 19370, 'Estimator', '57586 Forest Trail', 'El Banco', 'ctomini2@diigo.com');
insert into clients (id, nombre, apellido, telefono, oficio, direccion, ciudad, email) values (4000, 'Jasmina', 'Gounet', 11442, 'Supervisor', '5170 Gina Center', 'Jiucaizhuang', 'jgounet3@nymag.com');
insert into clients (id, nombre, apellido, telefono, oficio, direccion, ciudad, email) values (5000, 'Eva', 'Purry', 16283, 'Construction Expeditor', '77274 Dahle Street', 'Dhalie', 'epurry4@a8.net');
insert into clients (id, nombre, apellido, telefono, oficio, direccion, ciudad, email) values (6000, 'Laurella', 'Dallmann', 17582, 'Project Manager', '90895 Towne Street', 'Sovetskaya', 'ldallmann5@goodreads.com');
insert into clients (id, nombre, apellido, telefono, oficio, direccion, ciudad, email) values (7000, 'Tamiko', 'Bowra', 14312, 'Project Manager', '0569 Dayton Crossing', 'Gongxi', 'tbowra6@dedecms.com');
insert into clients (id, nombre, apellido, telefono, oficio, direccion, ciudad, email) values (8000, 'Andeee', 'Treanor', 17598, 'Construction Worker', '58475 Hoffman Road', 'Lebedyn', 'atreanor7@live.com');
insert into clients (id, nombre, apellido, telefono, oficio, direccion, ciudad, email) values (9000, 'Rosella', 'Murthwaite', 13139, 'Architect', '20000 Forster Plaza', 'Bar', 'rmurthwaite8@ca.gov');
insert into clients (id, nombre, apellido, telefono, oficio, direccion, ciudad, email) values (10000, 'Bette-ann', 'Elsbury', 18097, 'Construction Foreman', '26793 Sullivan Junction', 'Mataya', 'belsbury9@opera.com');